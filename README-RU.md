Почтовый сервер.
================
* Данный проект основан на инструкции [ISPMail guide](https://workaround.org/ispmail/) и проекте [Docker-mailserver](https://github.com/jeboehm/docker-mailserver). 
* Я добавил в проект ```PostfixAdmin``` и Адресную книгу на ```OpenLDAP```. Особенностью своей реализации считаю **Адресную книгу**. Она автоматически обновляется при появлении и удалении пользователей в базе ```PostfixAdmin```. Автоматически создается alias **Всем пользователям Домена**, который создает в базе ```PostfixAdmin``` одноименный alias. 
* Также реализовано автоматическое получение и обновление сертификата **LetsEncript**, использован проект [NGINX-LE - Nginx web and proxy with automatic let's encrypt](https://github.com/nginx-le/nginx-le).
* Отделный контейнер **restarter** по ```Cron``` раз в сутки перезапускает контейнеры **openldap**, **postfix**, **dovecot** для обновления сертификата.
* Я переработал и обновил контейнеры `postfix`, `dovecot`, `filter`(`Rspamd`) , `virus`(`Clamav`).

Функции
-------
* **Получение писем** для всех ваших виртуальных почтовых доменов.
* **Фильтрация** спама и вредоносных программ.
* **Отправка** писем.
* **Шифрование** всех соединений (по возможности).
* Добавление **DKIM подписи** к отправляемым письмам.
* **Хранение писем** на диске сервера.
* Использование **квот** (ограничение размера почтовых ящиков) пользователей.
* Разрешение пользователям управлять **фильтрации писем**, распределять письма по разным папкам, перенаправлять письма, или отправлять уведомления об отсутствии на рабочем месте.
* Предоставление **Webmail** доступа к почте.

И все это в отдельных контейнерах Docker.

Предварительная настройка.
--------------------------

Прежде чем начать установку почтового сервера, необходимо настроить DNS хотя бы для одного домена.

**Имя хоста почтового сервера.**

Почтовый сервер должен иметь уникальное имя хоста. Это имя является полным доменным именем и указывает на Ip адрес вашего сервера.
Например, если ваш почтовый домен называется ```example.org```, то имя хоста почтового сервера может называться ```mail.example.org```.
Убедитесь что в DNS есть запись для вашего имени хоста 

```mail.example.org. IN A x.x.x.x```

где ```x.x.x.x``` это IP адрес внешнего интерфейса вашего сервера. Исключением является сервер в DMZ. В этом случае вы должны использовать перенаправление портов с публичного IP адреса на ваш сервер.

**MX запись.**

Для каждого вашего почтового домена в DNS должна быть MX запись. С помьщью этой записи другие почтовые сервера определяют имя хоста вашего почтового сервера. А потом имя хоста разрешается в IP адрес с помощью записи A в DNS. 

Для домена ```example.org``` MX запись будет выглядеть так 

```example.org. IN MX 10 mail.example.org.```

Для другого вашего домена ```mydomen1.com``` MX будет выглядеть так 

```mydomen1.com IN MX 10 mail.example.org.```

**Запись в обратной зоне DNS.**

Для почтового сервера настоятельно рекомендуется настроить запись в обратной зоне DNS. Если ваше DNS имя ```mail.example.org``` разрешается в IP ```x.x.x.x```, то и ваш IP ```x.x.x.x``` должен разрешаться в DNS имя ```mail.example.org```

Проверить это можно командой ```nslookup x.x.x.x```

Запись в обратной зоне доступна к изменению владельцу IP адреса, обычно это ваш провайдер. Если не настроить правильно обратную зону, большинство почтовых серверов будут отмечать ваши письма как SPAM.

**DKIM, SPF, DMARC записи.**

DKIM запись для доменов будет создана в процессе установки. Посмотреть ее можно командой ```cat filter/dkim/mail.pub```

```mail._domainkey IN TXT "v=DKIM1; k=rsa; ""p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA72iwPIqgo7Q91GEgNPafzwavpIYW0pm5Lm6T7Z65AcRS6R/acAZUaCLJiqZp8LpUwiUnjsCo0r2rOwMEGO83BQoe3kXdGUdgnngOYvS+AiQP5QOd9vZfYkhgAMrR9KAdl9dMaq1cd9PkxV4aQRwu2xiuXJfDZIMtSCn/25MaaiCUROw7VZmi/J/As/z1ya1lfXGS6Iy2yC0zbrO8G""wMoeM4YqyabONDJx4CpkynQ7WOexu27SClalNv0tIme7xSyKK2KDBf4ZmbUyWaZzz4/dDKUFJs3JAolFFoo/JGr7rI7d/JSO0ruUJyPM2pwv0NcO6E+0D28VVbAnWK3X7uQMQIDAQAB"```

SPF запись может выглядеть так
```@ IN TXT "v=spf1 a mx ip4:x.x.x.x ~all"```

DMARC запись
```_dmarc IN TXT "v=DMARC1;p=none"```

Установка
---------

1. Запустим ```git clone https://gitlab.com/argo-uln/docker-mail-server.git```
2. Перейдем в директорию ```cd docker-mail-server```
3. Запустим файл предварительной настройки ```./setup.sh```
4. На предложение ```Please enter MYSQL PASSWORD:``` (введите пароль для mysql)
5. ```Please enter FQDN hostname:``` (введите полное имя хоста, пример mail.example.org)
6. ```Please enter mail domain:``` (введите имя вашего домена, пример example.org)
7. ```Please enter RSPAMD web access password:``` (введите пароль для вебинтерфейса RSPAMD)
8. ```Please enter LDAP user admin password:``` (введите пароль пользователя admin для Openldap)
9. `Please enter LDAP user adressbook password:` (введите пароль для LDAP пользователя `addressbook` - имеет доступ на чтение адресной книги)
10. ```Please enter the ip addresses separated by a space to access the services postfixadmin, rspamd. Default, access to them is closed:``` (введите ip адреса через пробел с которых будет предоставлен доступ к postfixadmin, rspamd, phpldapadmin)
11. Далее будет предложено ```You can start the build and run with the command: docker-compose up -d``` (запустите команду для установки и настройки сервиса почтового сервера)

> **Разберем что же делает `setup.sh`.**
> - В процессе работы ```setup.sh``` установится пакет ```php7.4-cli``` (если не был установлен ранее).
> - В корне сервера будет создана папка для почтовых ящиков ```/mail```, права на папку 777.
> - Из шаблона ```.env.dist``` будет создан файл переменных окружения ```.env```.
> - Из шаблона ```nginx/service.conf.dist``` будет создан ```nginx/service.conf```. В него будут добавлены ваши ip адреса для доступа к сервисам ```PostfixAdmin```, ```Rspamd```.
> - Из шаблона `openldap/ldif/addressbook.ldif.dist` будет создан файл `openldap/ldif/addressbook.ldif`
> - Из шаблона `openldap/ldif/ldap-add-ou-addressbook.ldif.dist` будет создан файл `openldap/ldif/ldap-add-ou-addressbook.ldif`
> - Из шаблона `openldap/ldif/oclAccess.ldif.dist` будет создан файл `openldap/ldif/oclAccess.ldif`
> - Из шаблона ```openldap/conf/mysql_ldap.php.dist``` будет создан файл ```openldap/conf/mysql_ldap.php```. Этот файл будет запускаться один раз в час. Он считывает из базы ```Mysql``` домены, пользователей. Создает в ```Openldap``` адресную книгу, алиас ```Всем пользователям домена```. В базе ```Mysql``` создается алиас ```Всем пользователям домена```. В случае удаления пользователей адресная книга в ```Openlpad``` обновляется. Так же обновляется алиас ```Всем пользователям домена``` в базе ```Mysql```.
> - Из шаблона ```roundcube/config/ldap.php.dist``` будет создан файл конфигурации ```roundcube/config/ldap.php```
> - Из шаблона ```roundcube/config/managesieve.php.dist``` будет создан файл конфигурации ```roundcube/config/managesieve.php```
> - Из шаблона ```postfixadmin/config.local.php.dist``` будет создан файл конфигурации ```postfixadmin/config.local.php```.
> - Из шаблона `init/roundcube.sql.dist` будет создан файл `init/roundcube.sql`.

12. Запустим команду ```docker-compose up``` и подождем, пока контейнеры соберутся и запустятся.
13. Откройте в браузере страницу https://mail.example.org/postfixadmin/setup.php (где mail.example.org замените на имя вашего сервера)
14. Введите пароль для mysql.
![Введите пароль для mysql](https://gitlab.com/argo-uln/docker-mail-server/-/raw/main/img/setup_password.png) 
15. Прокрутите окно браузера вниз. Введите еще раз пароль для mysql. Создайте пользователя администратор postfixadmin. Есть ограничения. Пароль должен содержать не менее 5 символов (не мене 3 букв, 2 цифр, одного спец символа). Имя пользователя, что то типа admin@example.org (подставьте свой домен).
![Создайте пользователя администратор postfixadmin](https://gitlab.com/argo-uln/docker-mail-server/-/raw/main/img/add_superadmin_acc.png)
16. Зайдите по ссылке https://mail.example.org/postfixadmin/login.php.
17. Создайте почтовый домен example.org. Создайте пользователя admin@example.org. Пароль должен содержать не менее 5 символов (не мене 3 букв, 2 цифр, одного спец символа) 
18. Откройте web интерфейс почты https://mail.example.org/mail/. Логин: admin@example.org. 
19. Для доступа к серверу RSPAMD откройте страницу https://mail.example.org/rspamd/ и введите пароль пользователя mysql.
20. Пропишите **dkim** запись в dns зонах всех ваших доменов. Для получения ключа запустите команду ```cat filter/dkim/mail.pub```
```
mail._domainkey IN TXT ( "v=DKIM1; k=rsa; "
	"p=ваш-dkim-ключ" ) ;
```
21. Вы можете проверить ваш ключ на сервисе https://dkimcore.org/tools/keycheck.html

* В поле ```Selector:``` mail
* В поле ```Domain name:``` example.org
* Нажмите - ```Check```

***Обратите внимание:*** Мы используем один ключ для всех наших доменов.

22. Для доступа к серверу ```RSPAMD``` откройте страницу https://mail.example.org/rspamd/
Введите пароль для доступа на станицу. 
23. Перейдите на вкладку ```Сonfiguration```.
24. Настройки:
* ```Greylist```        4
* ```Probably Spam```   6
* ```Rewrite subject``` 7
* ```Spam```            150 (Письма гарантированно пройдут и попадут в папку "Спам")
25. Сохраните изменения. ```Save action```, ```Save cluster```. 

***Обратите внимание:*** В случае обнаружения вируса в письме, оно будет отброшено.

Переменные окружения.
---------------------
После запуска файла предварительной настройки ```./setup.sh``` вас попросят ввести 

`MYSQL PASSWORD`, `FQDN hostname`, `mail domain`, `RSPAMD web access password`, `LDAP user admin password`, `LDAP user adressbook password`, `ip address`.

* Из шаблона .env.dist будет создан файл переменных окружения .env
* Значение `MYSQL PASSWORD` подставится в переменные `POSTFIXADMIN_DB_PASSWORD`, `PA_SETUP_PASS`, `MYSQL_ROOT_PASSWORD`, `MYSQL_PASSWORD` файла .env. Также из значения `MYSQL PASSWORD` генерится переменная `PA_SETUP_HASH` для файла конфигурации postfixadmin/config.local.php
* Значение `FQDN hostname` подставится в переменные `MAILNAME`, `LE_FQDN`, `MAIL_HOST`, `ROUNDCUBEMAIL_SMTP_SERVER`, `ROUNDCUBEMAIL_DEFAULT_HOST`файла .env. Также значение `FQDN hostname` будет использовано в файле roundcube/config/managesieve.php Из `FQDN hostname` будут сгенерированы переменные LDAP `BASEDN` и `ROOTDN`. Они используются в файле `openldap/conf/mysql_ldap.php`, который создает и обновляет `Адресную книгу` и алиас `Всем пользователям домена` в `mysql`
* Значение `mail domain` подставится в переменные `DOMAIN`, `POSTMASTER`, `LE_EMAIL` файла .env и в файле postfixadmin/config.local.php
* `RSPAMD web access password` подставится в переменную `CONTROLLER_PASSWORD` файла .env
* `LDAP user admin password` подставится в переменную `LDAP_PASS` файла .env, используется файле `openldap/conf/mysql_ldap.php` и в файле `roundcube/config/ldap.php`
* `LDAP user adressbook password` подставится в переменную `ADDRESSBOOK_PASS` файла .env, используется в файле `openldap/ldif/addressbook.ldif
* `ip address` добавится в файл nginx/service.conf и откроет доступ с этих IP к сервисам Postfixadmin, RSPAMD

Отдельные контейнеры, используемые в проекте:
---------------------------------------------
| Контейнер | Описание | Источник | Порты |
| ------ | ------ | ------ | ------ |
| db | База данных для Postfixadmin, Roundcube | В проекте используется стандартный образ mariadb | port 3306(снаружи недоступно) |
| postfixadmin | Web интерфейс для администрирования почтового сервера | В проекте используется стандартный образ postfixadmin | port 443, https://mail.example.org/postfixadmin/login.php |
| nginx | Прокси сервер с поддержкой получения сертификатов LetsEncript, прокси для postfixadmin (доступ ограничен по ip), прокси для Roundcube (web клиент почты), прокси для web интерфейса RSPAMD (доступ ограничен по IP) | использован контейнер umputun/nginx-le:latest |port 80 для подтверждения домена LetsEncript, port 443 прокси к сервисам postfixadmin, Roundcube, RSPAMD |
| postfix | smtp сервер | build: postfix | только 25 порт |
| dovecot | IMAP, POP3, Submission, LMTP, Sieve сервер | build: dovecot | imap 110, imaps 143(STARTTLS), submission 587(STARTTLS), imaps 993(SSL), pop3s 995, lmtp port 2003 (снаружи недоступно), managesieve 4190 (STARTTLS) |
| filter | Сервер Rspamd | build: filter | port 11332 для общения с Postfix(снаружи недоступно), Web сервер статистики port 11334, снаружи https://mail.example.org/rspamd/ |
| redis | Сервер БД redis для Rspamd | Использован образ redis:alpine | port 6379, снаружи недоступно |
| virus | Сервер антивирус Clamav | build: virus | port 3310 для общения с Rspamd(снаружи недоступно) |
| roundcube | Веб клиент к электронной почте | использован образ roundcube/roundcubemail:1.3.17-apache | снаружи port 443, https://mail.example.org/mail/ |
| openldap | Openldap сервер | build: openldap | port 636 |
| restarter | Cron раз в сутки перезапускает контейнеры openldap, postfix, dovecot (для обновления сертификата LetsEncript) | использован image: docker | |

Web интерфейс доступа к почте Roundcube (дополнительные возможности).
---------------------------------------------------------------------

**Откройте web интерфейс почты https://mail.example.org/mail/.**

1. Фильтры Sieve. 

Откройте `Настройки`, `Фильтры`. Во вкладке `Фильтры` нажмите `+`. Появится окошко создания фильтра. Доступно многое, а именно возможность распределять письма по разным папкам, перенаправлять письма, или отправлять уведомления об отсутствии на рабочем месте. После создания фильтра нажмите `сохранить`.

2. Адресная книга LDAP. 

Нажмите в меню `Контакты`. Отобразится адресная книга LDAP. Обновляется один раз в час. В контактах отобразятся `пользователи` всех ваших доменов и алиасы `Всем пользователям домена`.

Настройка Thunderbird
---------------------

**Параметры сервера**

> - Тип сервера: **Почтовый сервер IMAP** 
> - Имя сервера: `mail.example.org` Порт: `143`
> - Имя пользователя: `admin@example.org`
> - Защита соединения: `STARTTLS`
> - Метод аутентификации: `Обычный пароль`

**Сервер исходящей почты**

> - Тип сервера: **SMTP-сервер** 
> - Имя сервера: `mail.example.org` Порт: `587`
> - Имя пользователя: `admin@example.org`
> - Защита соединения: `STARTTLS`
> - Метод аутентификации: `Обычный пароль`


**Адресная книга** (Файл, создать, каталог LDAP)

> **_Вкладка Основное_**
> - Название: `mail.example.org`
> - Имя сервера: `mail.example.org`
> - Корневой элемент (Base DN): `ou=addressbook,dc=mail,dc=example,dc=org`
> - Порт: `636`
> - Имя пользователя (Bind DN): `cn=addressbook,dc=mail,dc=example,dc=org` (вас попросят ввести пароль - это значение переменной `ADDRESSBOOK_PASS` файла .env)
> - `X` Использовать защищенное соединение (SSL)
> 
> **_Вкладка Дополнительно_**
> 
> - Не возвращать более чем `1000` результатов
> - Область поиска `Поддерево`
> - Фильтр поиска `(mail=*)`
> - Способ аутентификации `простой`

Как это работает?
-----------------
**Рассмотрим пример, когда пользователь хочет отправить письмо в интернет.**
![Пользователь отправляет письмо в интернет](https://gitlab.com/argo-uln/docker-mail-server/-/raw/main/img/submission.png)
1. Пользователь подключается по защищенному соединению к Dovecot на порт submission 587(STARTTLS). 
2. Dovecot проверяет имя пользователя и пароль в базе данных. 
3. Если пользователь опознан, то сообщение перенаправляется на отправку в Postfix.
4. Postfix отправляет сообщение на проверку в RSPAMD.
5. RSPAMD хранит свои данные в redis.
6. RSMAMD проверяет письмо на вирусы (если обнаружен вирус отправка письма прекращается).
7. После проверки письмо возвращается в Postfix.
8. Postfix отправляет письмо на почтовый сервер получателя.

**Рассмотрим пример получения письма из интернета.**
![Получение письма и интернета](https://gitlab.com/argo-uln/docker-mail-server/-/raw/main/img/mail-in.png)

1. Удаленный почтовый сервер подключается к нашему серверу Postfix по протоколу SMTP.
2. Postfix проверяет в базе данных домен получателя. Если это наш виртуальный домен, то проверяет имя пользователя.
3. Если письмо адресовано нашему пользователю, то оно отправляется на проверку в RSPAMD.
4. RSPAMD хранит свои данные в redis.
5. RSMAMD проверяет письмо на вирусы (если обнаружен вирус письмо отбрасывается).
6. После проверки письмо возвращаеся Postfix.
7. Postfix отправляет письмо в Dovecot для локальной доставки в ящик пользователя.
8. Dovecot проверяет дополнительные правила Sieve и сохраняет письмо в локальный ящик пользователя.

Sieve - правила фильтрации, используемые Dovecot.
-------------------------------------------------

1. Письма помеченные как спам помещаются в папку Spam в ящике пользователя.
2. Если письмо переносится пользователем в папку Spam, происходит обучение спам фильтра.
3. Если письмо из папки Spam переносится в другую папку, то также происходит обучение спам фильтра. 

Как включить проверку типов файлов на уровне Postfix?
-----------------------------------------------------

1. Отредактируйте файл `docker-mail-server/postfix/rootfs/etc/postfix/mime_header_checks`

`/name=[^>]*\.(bat|com|exe|dll|vbs|docm|doc|dzip)/ REJECT`

2. В файле `.env.dist` исправьте значение переменной `FILTER_MIME=true`
3. Пересоздайте файл `.env` запустив ./setup.sh, или исправьте `FILTER_MIME=true` в файле `.env`.
4. Потребуется пересоздать контейнер postfix

RECIPIENT_DELIMITER
-------------------

Значение по умолчанию этого параметра в .env `RECIPIENT_DELIMITER=-`
Это позволяет вам отправлять письма снаружи на `user-всечтоугодно@example.com`. Письмо будет доставлено в ящик `user@example.com`.


Как сделать резервную копию баз контейнера db?
-----------------------------------------

Из директории docker-mail-server запустите команду

`source .env && docker-compose exec db mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} --all-databases > db-dump-$(date +%F_%H-%M-%S).sql`

Как восстановить базы  из архива?
---------------------------------

`source .env && docker-compose exec -T db mysql -uroot -p${MYSQL_ROOT_PASSWORD} < mariadb-dump.sql`

Утилита IMAPSync
----------------

Помощь при переносе почтовы ящиков с одного почтового сервера на другой. Протестировано на `Ubuntu 20.04`. Взято https://www.ylsoftware.com/news/733

> Утилиту необходимо запускать на отдельном хосте.

1. Установим зависимости.

```
sudo apt install  \
libauthen-ntlm-perl     \
libclass-load-perl      \
libcrypt-ssleay-perl    \
libdata-uniqid-perl     \
libdigest-hmac-perl     \
libdist-checkconflicts-perl \
libencode-imaputf7-perl     \
libfile-copy-recursive-perl \
libfile-tail-perl       \
libio-compress-perl     \
libio-socket-inet6-perl \
libio-socket-ssl-perl   \
libio-tee-perl          \
libmail-imapclient-perl \
libmodule-scandeps-perl \
libnet-dbus-perl        \
libnet-ssleay-perl      \
libpar-packer-perl      \
libreadonly-perl        \
libregexp-common-perl   \
libsys-meminfo-perl     \
libterm-readkey-perl    \
libtest-fatal-perl      \
libtest-mock-guard-perl \
libtest-mockobject-perl \
libtest-pod-perl        \
libtest-requires-perl   \
libtest-simple-perl     \
libunicode-string-perl  \
liburi-perl             \
libtest-nowarnings-perl \
libtest-deep-perl       \
libtest-warn-perl       \
make                    \
cpanminus
```
2. Скачаем imapsync `git clone https://github.com/imapsync/imapsync.git`
3. `cd imapsync`
4. Создадим файл `mails.csv`

`old_login|old_pass|new_login|new_pass|`

Где Здесь "old_login" и "old_pass" - логин и пароль пользователя на старом сервере, а "new_login" и "new_pass" - логин и пароль пользователя на новом сервере.

5. Создадим файл `sync.sh`
6. Дадим права на запуск`chmod +x sync.sh`
7. Содержимое файла sync.sh

```
#!/bin/bash

cd `dirname $0`

for line in `cat mails.csv | grep -v ^#`; do
        M_USER=`echo ${line} | cut -d '|' -f1`
        M_PASS=`echo ${line} | cut -d '|' -f2`
        N_USER=`echo ${line} | cut -d '|' -f3`
        N_PASS=`echo ${line} | cut -d '|' -f4`
        echo "Processing ${M_USER}..."
        ./imapsync \
                --host1 mail.company.com:993    --user1 ${M_USER} --password1 ${M_PASS} --tls2\
                --host2 mail.example.org:143  --user2 ${N_USER} --password2 ${N_PASS} --tls2
        if [ $? -ne "0" ]; then
                echo ${M_USER} >> mail_errors
        fi
done
```
8. После выполнения скрипта в файл "mail_errors" будут записаны логины пользователей, для которых перенос почты не удался.


Если что-то пошло не так.
------------------------

* Из директории docker-mail-server запустите команду ```docker-compose down```
* Потом ```docker system prune -a --volumes``` очистка всех остановленных контейнеров и неиспользуемых сетей
* Поправьте ошибки и запустите ```docker-compose up -d```
* Пересоздавать пользователей в этом случае нет необходимости, просто подтвердите пароль, выполнив инструкцию в пункте ниже
* Откройте в браузере страницу https://mail.example.org/postfixadmin/setup.php (где ```mail.example.org``` замените на имя вашего сервера)
* Введите пароль mysql
* Для полного удаления всех данных удалите папку /mail и директорию с проектом docker-mail-server
* Посмотреть логи ```docker-compose logs -f```

HOWTO
-----

* При создании нового домена в `PostfixAdmin` автоматически создаются адреса алиасы `abuse@newdomain`, `hostmaster@newdomain`, `postmaster@newdomain`, `webmaster@newdomain`. Они автоматически переадресовывают почту на основновной домен, на почтовый ящик `admin@example.org`
